<div class="row">
    <div class="col-sm-6 col-sm-offset-3">
        <h3>Contact</h3>
        <table class="table table-bordered table-hover">
            <tr>
                <td>Address</td>
                <td>037 Bo. Green, 9700, Marawi City, Lanao del Sur, Philippines</td>
            </tr>
            <tr>
                <td>Phone</td>
                <td>0939 622 5097</td>
            </tr>
            <tr>
                <td>Emails</td>
                <td>info@pmtc.edu.ph | noralinador@gmail.com</td>
            </tr>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <h1 class="text-center">Extension Services</h1>
        <hr />
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-sm-6 ">
        <div class="panel panel-success">
            <div class="panel-heading">
                <strong>Charitable Projects and Other Extension Services</strong>
            </div>
            <div class="panel-body">
                <ul>
                    <li>PMTC Mahad Fatimah Al-Islamie</li>
                    <li>Masjid Masiu Islamic Center</li>
                    <li>Jamiatu Nissa Litahmil Qur'an Fi Mindanao</li>
                    <li>Fatimah Orphanage Home</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="col-md-6 col-sm-6 ">
        <div class="panel panel-success">
            <div class="panel-heading">
                <strong>Educational Programs</strong>
            </div>
            <div class="panel-body">
                <ul>
                    <li>PMTC Computer Literacy Program</li>
                    <li>PMTC Model Pre-School</li>
                    <li>PMTC Review & Training Center</li>
                    <li>Educational Consultancy</li>
                    <li>Seminars/Assembly & Live-in Trainings</li>
                    <li>Maranao Rawatun Cultural Council</li>
                </ul>
            </div>
        </div>
    </div>

</div>
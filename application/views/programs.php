<div class="row">
    <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
        <h2 class="text-center">Academic Programs</h2>
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
        <div class="list-group">
            <a href="<?=base_url('pmtc/programs/#');?>" class="list-group-item">Bachelor of Elementary Education</a>
            <a href="<?=base_url('pmtc/programs/#');?>" class="list-group-item">Bachelor of Secondary Education</a>
            <a href="<?=base_url('pmtc/programs/#');?>" class="list-group-item">Certificate in Professional Teaching</a>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
        <h2 class="text-center">Admission Requirements</h2>
        <p class="text-info">The following admission requirements are prescribed for incoming freshmen and transferees</p>
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
        <ul>
            <li>Orignal Copy of Report Card (Form 137-A)</li>
            <li>Certificate of Good Moral Character</li>
            <li>Transcript of Records (Transferees)</li>
            <li>4 copies of 1x1 latest ID pictures</li>
            <li>Interview/ Admission Exam</li>
            <li>Birth Certificate</li>
            <li>Marriage Contract (if applicable)</li>
        </ul>
    </div>
</div>
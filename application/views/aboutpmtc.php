<div class="row">
    <div class="col-md-12">
        <div class="col-lg-12"><h1 class="text-center">Philippine Muslim Teachers' College</h1><hr /></div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="col-md-4 col-md-offset-4"><img class="img-responsive" src="<?=base_url('/images/pmtclogo.png');?>"/></div>
    </div>
</div>

<hr />

<div class="row">
    <div class="col-md-12">
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    About PMTC
                </div>
                <div class="panel-body">
                    <div class="list-group">
                        <a href="#philosophy" class="list-group-item">Philosophy</a>
                        <a href="#mission" class="list-group-item">Mission and Vision</a>
                        <a href="#goals" class="list-group-item">Goals and Objectives</a>
                        <a href="<?=base_url('photogalleries');?>" class="list-group-item">Photo Galleries</a>
                    </div>
                    <div class="list-group">
                        <a href="#" class="list-group-item">Board of Directors</a>
                        <a href="#" class="list-group-item">Management and Staff</a>
                    </div>
                </div>
            </div>

            <div class="panel panel-info">
                <div class="panel-heading">
                    Contact
                </div>
                <div class="panel-body">
                    <p>
                        Philippine Muslim Teachers' College <br />
                        037 Bo. Green, 9700, Marawi City<br />
                        Lanao del Sur<br />
                        Philippines
                    </p>
                </div>
            </div>


        </div>
        <div class="col-md-8 pmtc-background">
            <h3 id="background">Background</h3>
            <p>The Autonomous Region in Muslim Mindanao (ARMM) has been facing a great deficiency of qualified and competent teachers in the field. The quality of teachers and graduates produced by various colleges & universities in the Muslim Region could hardly answer to the educational needs, demands & aspirations of its people.</p>
            <p>The establishment of the Philippine Muslim Teacher's College (PMTC) Foundation, Inc., introduces the newly-designed curriculum for teachers education with milti-discipline course meant for three (3) year-completion on trimester program. This is a result of a long term survery research and comprehensive educational studies initiated by the CHED-ARMM.</p>
            <p>This Teacher Education curriculum of PMTC aims to produce new breed of teachers who shall play as catalysts of change for a better Muslim Ummah.</p>
            <p>The PMTC foundation addresses the common prevailing issues on Education. The following are the significant & urgent factors being addressed by the PMTC Three (3) year Trimester Education program:</p>
            <ul>
                <li>With, the seemingly - unending war in Mindanao, this help to speed up development of manpower resources who are capable, rational & responsible, so that they can improve their standard of life & contribute in the rebuilding of Mindanao & the development of the country as a whole.</li>
                <li>Seems the period of course completion is short, this makes it less cost-effective for the students & the parents who barely have no money to spend on good & it is time saving for early employment.</li>
            </ul>

            <hr />
            <h3 id="philosophy">Philosophy</h3>

            <p>The Philippine Muslim Teachers' College aims to be recognized as the premium educational institution in Mindanao at par in excellence with the country's leading learning institution offering tertiary education.</p>
            <p>It is in its ideology to create the most effective, responsive and relevant educational system for Muslims in the Southern Philippines by fostering the holistic growth of each individual Muslim including his/her physical, intellectual, moral, social and spiritual development.</p>

            <hr />
            <h3 id="mission">Mission</h3>
            <ul>
                <li>To produce God-fearing professionals with sense of social accountability and who can demonstrate excellence and competence in their chosen career.</li>
                <li>To inculcate the teachings of Islam, to each student and make use of it as a guide for a successful, blessed, happy existence.</li>
                <li>To instill respect for human rights, love of humanity, peace, unity, knowledge, hard work and discipline.</li>
                <li>To mold each individual the desire for progress so that he/she could contribute effectively in the improvement of his/her family, then to the rebuilding of Mindanao and to the development of the country.</li>
            </ul>

            <hr />
            <h3 id="vision">Vision</h3>

            <p>To realize its mission, the Philippine Muslim Teachers' College Foundation is committed and dedicated to serve its immediate community, particularly the constituents of the Autonomous Region in Muslim Mindanao. These include the four (4) component provinces namely, Lanao del Sur, Maguindanao, Sulu and Tawi-Tawi and the component cities of ARMM such as, the Islamic City of Marawi, the City of Cotabato and also including non-ARMM neighboring provinces.</p>

            <hr />
            <h3 id="goals">Goals and Objectives</h3>
            <ul>
                <li>To produce trained, competent, quality teachers of high calibre of sufficient numbers to meet the requirements of all types of schools within the educational system -- primary, secondary, vocational/technical and tertiary.</li>
                <li>To improve continuously the knowledge, attitude and skills of professional teachers or academicians.</li>
                <li>To build up student's confidence in his/her own ability, creativity, innovativeness, and sensitivity to the peace and order problem in Mindanao, and preservation of his/her own cultural identity.</li>
                <li>To provide relevant applicable and adequate learning experiences for the Muslim students.</li>
                <li>To respond to the current educational challenges in the Philippines particularly in Muslim Mindanao.</li>
                <li>To lead educational innovativeness that are relevant to the demands of our Philippine society and the Muslim communities in particular.</li>
                <li>To explore the power of Information TEchnology and its application to different disciplines.</li>
                <li>To support the personal and professional growth of students regardless of background.</li>
                <li>To implement a newly designed and effective curriculum utilizing the capabilities of Information Technology.</li>
                <li>To develop and implement a systematic evaluation scheme for teachers and improve teaching and supervisory effectiveness, approaches, methods and techniques.</li>
                <li>To provide continuous upgrading of teachers' competence and school administration.</li>
                <li>To respond effectively to the changing needs and condition of our people through a new approach of innovative curriculum.</li>
                <li>To link education toward the individuals' ultimate goal for successful achievement.</li>
                <li>To establish an effective and efficient educational background, experiences and exposures for Muslim teachers.</li>
                <li>To lead in further research on the answer to the best and most appropriate education for Mindanao and particularly in ARMM</li>
            </ul>
        </div>
    </div>
</div>

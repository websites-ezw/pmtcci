<div class="row bod">
    <div class="col-md-12 col-sm-12">
        <h1 class="text-center">Board of Directors</h1>
    </div>
</div>
<hr />
<div class="row text-center bod">
    <div class="col-md-12 col-sm-12">
        <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
            <img src="<?=base_url('images/bod/drnoralin.jpg');?>" class="img-responsive bod-photo img-thumbnail" />
        </div>
        <div class="col-md-12 col-sm-12">
            <h3 class="bod-name">Dr. Noralin Mangondato Sharief-Ador</h3>
            <h4 class="bod-position">Chairman of the Board</h4>
        </div>
    </div>
</div>
<hr />
<div class="row bod">
    <div class="col-md-12 col-sm-12">
        <div class="col-md-6 col-sm-6 text-center">
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12 ">
                <img src="<?=base_url('images/placeholder/photo.jpg');?>" class="img-responsive bod-photo img-thumbnail" />
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <h3 class="text-center">Agakhan M. Sharief</h3>
                <h4 class="text-center">Vice Chairman/ Acting President</h4>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 text-center">
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12 ">
                <img src="<?=base_url('images/placeholder/photo.jpg');?>" class="img-responsive bod-photo img-thumbnail" />
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <h3 class="text-center">Sophia A. Ampuan-Sharief</h3>
                <h4 class="text-center">Founding President</h4>
            </div>
        </div>
    </div>
</div>
<hr />
<div class="row text-center bod">
    <div class="col-md-12 col-sm-12">
        <h2>Director Members</h2>
        <hr />
    </div>
    <div class="col-md-12 col-sm-12 directormembersrow">
        <div class="col-md-4 col-sm-4">
            <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
                <img src="<?=base_url('images/placeholder/photo.jpg');?>" class="img-responsive bod-photo img-thumbnail" />
            </div>
            <div class="col-md-12 col-sm-12">
                <h3>Okile M. Sharief</h3>
            </div>
        </div>
        <div class="col-md-4 col-sm-4">
            <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
                <img src="<?=base_url('images/placeholder/photo.jpg');?>" class="img-responsive bod-photo img-thumbnail" />
            </div>
            <div class="col-md-12 col-sm-12">
                <h3>Omar-Ali M. Sharief</h3>
            </div>
        </div>
        <div class="col-md-4 col-sm-4">
            <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
                <img src="<?=base_url('images/placeholder/photo.jpg');?>" class="img-responsive bod-photo img-thumbnail" />
            </div>
            <div class="col-md-12 col-sm-12">
                <h3>Abdul Jabbar M. Sharief</h3>
            </div>
        </div>
    </div>
    <div class="col-md-12 directormembersrow">
        <div class="col-md-4 col-sm-4">
            <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
                <img src="<?=base_url('images/placeholder/photo.jpg');?>" class="img-responsive bod-photo img-thumbnail" />
            </div>
            <div class="col-md-12 col-sm-12">
                <h3>Lucky Sultan M. Sharief</h3>
            </div>
        </div>
        <div class="col-md-4 col-sm-4">
            <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
                <img src="<?=base_url('images/placeholder/photo.jpg');?>" class="img-responsive bod-photo img-thumbnail" />
            </div>
            <div class="col-md-12 col-sm-12">
                <h3>Dayamon Guiling-Sharief</h3>
            </div>
        </div>
        <div class="col-md-4 col-sm-4">
            <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
                <img src="<?=base_url('images/placeholder/photo.jpg');?>" class="img-responsive bod-photo img-thumbnail" />
            </div>
            <div class="col-md-12 col-sm-12">
                <h3>Jadida Pangandaman-Sharief</h3>
            </div>
        </div>
    </div>
</div>

<hr id="mas"/>

<div class="row bod">
    <div class="col-md-12 col-sm-12">
        <h1 class="text-center">Management and Staff</h1>
    </div>
</div>
<hr />
<div class="row">
    <div class="col-sm-6 col-sm-offset-3 text-center mgmt-staff">

        <h4>Agakhan M. Sharief</h4>
        <p>Acting President</p>

        <h4>Name</h4>
        <p>Academic and Management Consultant</p>

        <h4>Name</h4>
        <p>Program Head Consultant</p>

        <h4>Name</h4>
        <p>Acting Registrar</p>

        <h4>Name</h4>
        <p>College Coordinator</p>

        <h4>Name</h4>
        <p>Research Assistant</p>

        <h4>Name</h4>
        <p>Librarian</p>

    </div>
</div>
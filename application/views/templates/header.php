<!DOCTYPE html>
<html lang="en"><head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?=base_url('images/favicon.ico');?>" type="image/x-icon">
    <link rel="icon" href="<?=base_url('images/favicon.ico');?>" type="image/x-icon">

    <?php if (isset($title)): ?>
        <title>PMTC | <?=$title;?></title>
    <?php else: ?>
        <title>PMTC</title>
    <?php endif; ?>

    <!-- Bootstrap core CSS -->
    <link href="<?=base_url()?>public/css/bootstrap-flatly-pmtc.min.css" rel="stylesheet">
    <link href="<?=base_url()?>public/css/font-awesome.css" rel="stylesheet">
    <link href="<?=base_url()?>public/css/flaticon.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?=base_url()?>public/css/full-slider.css" rel="stylesheet">
    <link href="<?=base_url()?>public/css/custom.css" rel="stylesheet">
    <link href="<?=base_url()?>public/css/colorbox.css" rel="stylesheet">


<body>

<div class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?=base_url();?>">PMTC</a>
        </div>
        <div class="navbar-collapse collapse">

            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">About PMTC <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?=base_url('pmtc/');?>">Background</a></li>
                        <li><a href="<?=base_url('pmtc/#philosophy');?>">Philosophy</a></li>
                        <li><a href="<?=base_url('pmtc/#mission');?>">Mission and Vision</a></li>
                        <li><a href="<?=base_url('pmtc/#goals');?>">Goals and Objectives</a></li>
                        <li><a href="<?=base_url('photogalleries');?>">Photo Galleries</a></li>
                        <li class="divider"></li>
                        <li class="dropdown-header">Organization</li>
                        <li><a href="<?=base_url('pmtc/bod');?>">Board of Directors</a></li>
                        <li><a href="<?=base_url('pmtc/bod/#mas');?>">Management and Staff</a></li>
                        <li class="divider"></li>
                        <li><a href="<?=base_url('pmtc/contact');?>">Contact</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Programs <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?=base_url('pmtc/programs');?>">Programs</a></li>
                        <li><a href="<?=base_url('pmtc/programs');?>">Bachelor of Elementary Education</a></li>
                        <li><a href="<?=base_url('pmtc/programs');?>">Bachelor of Secondary Education</a></li>
                        <li><a href="<?=base_url('pmtc/programs');?>">Certificate in Professional Teaching</a></li>
                    </ul>
                </li>
                <li><a href="<?=base_url('pmtc/extensionservices');?>">Extension Services</a></li>
<!--                <li><a href="--><?//=base_url('pmtc/careers');?><!--">Careers</a></li>-->
                <li><a href="<?=base_url('photogalleries');?>">Photo Galleries</a></li>

            </ul>
        </div><!--/.nav-collapse -->
    </div>
</div>

<div class="container" role="main" id="main">
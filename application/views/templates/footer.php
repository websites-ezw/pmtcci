</div> <!-- /container -->
<div id="footer" class="bg-primary">
    <div class="container">
        <div class="col-lg-12">
            <h4>&copy; Philippine Muslim Teachers' College</h4>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?=base_url()?>public/js/jquery.js"></script>
<script src="<?=base_url()?>public/js/jquery.colorbox.js"></script>
<script src="<?=base_url()?>public/js/bootstrap.js"></script>
<!-- Script to Activate the Carousel -->
<script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
</script>
<script>
    $(document).ready(function(){
        //Examples of how to assign the Colorbox event to elements
        $(".thumbnail").colorbox({rel:'group3', transition:"fade", width:"75%", height:"75%"});
    });
</script>
</body></html>
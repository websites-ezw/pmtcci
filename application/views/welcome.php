<div id="myCarousel" class="carousel slide">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        <div class="item active">
            <div class="fill"><img src="images/placeholder/banner1.jpg" class="img-responsive"/></div>
        </div>
        <div class="item">
            <div class="fill"><img src="images/placeholder/banner2.jpg" class="img-responsive" /></div>
        </div>
        <div class="item">
            <div class="fill"><img src="images/placeholder/banner3.jpg" class="img-responsive"/></div>
        </div>
        <div class="item">
            <div class="fill"><img src="images/placeholder/banner2.jpg" class="img-responsive"/></div>
        </div>
    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="icon-prev"></span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="icon-next"></span>
    </a>
</div>

<hr />

<div class="row">
    <div class="col-md-12">
        <div class="col-md-4 pmtclogosection">
            <div class="panel panel-default">
                <div class="panel-heading">
                    About PMTC
                </div>
                <div class="panel-body">
                    <div class="list-group">
                        <a href="<?=base_url('pmtc/#philosophy');?>" class="list-group-item">Philosophy</a>
                        <a href="<?=base_url('pmtc/#mission');?>" class="list-group-item">Mission and Vision</a>
                        <a href="<?=base_url('pmtc/#goals');?>" class="list-group-item">Goals and Objectives</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    Background
                </div>
                <div class="panel-body">
                    <p>The Autonomous Region in Muslim Mindanao (ARMM) has been facing a great deficiency of qualified and competent teachers in the field. The quality of teachers and graduates produced by various colleges & universities in the Muslim Region could hardly answer to the educational needs, demands & aspirations of its people.</p>
                    <p>The establishment of the Philippine Muslim Teacher's College (PMTC) Foundation, Inc., introduces the newly-designed curriculum for teachers education with milti-discipline course meant for three (3) year-completion on trimester program. This is a result of a long term survery research and comprehensive educational studies initiated by the CHED-ARMM.</p>
                    <p>This Teacher Education curriculum of PMTC aims to produce new breed of teachers who shall play as catalysts of change for a better Muslim Ummah.</p>
                    <p>The PMTC foundation addresses the common prevailing issues on Education. The following are the significant & urgent factors being addressed by the PMTC Three (3) year Trimester Education program:</p>
                    <ul>
                        <li>With, the seemingly - unending war in Mindanao, this help to speed up development of manpower resources who are capable, rational & responsible, so that they can improve their standard of life & contribute in the rebuilding of Mindanao & the development of the country as a whole.</li>
                        <li>Seems the period of course completion is short, this makes it less cost-effective for the students & the parents who barely have no money to spend on good & it is time saving for early employment.</li>
                    </ul>
                    <hr />
                    <div class="btn-group">
                        <a href="<?=base_url('pmtc/');?>" class="btn btn-primary">Read more</a>
                        <a href="<?=base_url('pmtc/#philosophy');?>" class="btn btn-info">Philosophy</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends MY_Controller {

    protected $_uri = 'Welcome';

	public function index()
	{
        $data['uri'] = base_url();
		$this->load->view('templates/header', $data);
		$this->load->view('welcome');
		$this->load->view('templates/footer');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
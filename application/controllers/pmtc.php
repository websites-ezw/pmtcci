<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class pmtc extends MY_Controller {

    protected $_uri = 'aboutpmtc';

    public function index()
    {
        $data['url'] = base_url('pmtc');
        $this->load->view('templates/header', $data);
        $this->load->view('aboutpmtc', $data);
        $this->load->view('templates/footer', $data);
    }

    public function bod()
    {
        $data['url'] = base_url('pmtc/bod');
        $this->load->view('templates/header', $data);
        $this->load->view('bod', $data);
        $this->load->view('templates/footer', $data);
    }

    public function mas()
    {
        $data['url'] = base_url('pmtc/mas');
        $this->load->view('templates/header', $data);
        $this->load->view('mas', $data);
        $this->load->view('templates/footer', $data);
    }

    public function contact()
    {
        $data['url'] = base_url('pmtc/contact');
        $this->load->view('templates/header', $data);
        $this->load->view('contact', $data);
        $this->load->view('templates/footer', $data);
    }

    public function programs()
    {
        $data['url'] = base_url('pmtc/programs');
        $this->load->view('templates/header', $data);
        $this->load->view('programs', $data);
        $this->load->view('templates/footer', $data);
    }

    public function extensionServices()
    {
        $data['url'] = base_url('pmtc/extensionServices');
        $this->load->view('templates/header', $data);
        $this->load->view('services', $data);
        $this->load->view('templates/footer', $data);
    }

}
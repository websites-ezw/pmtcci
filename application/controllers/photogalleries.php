<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class photoGalleries extends MY_Controller {

    public function index()
    {
        $data['url'] = base_url('photogalleries');
        $this->load->view('templates/header', $data);
        $this->load->view('photogalleries', $data);
        $this->load->view('templates/footer', $data);
    }

}
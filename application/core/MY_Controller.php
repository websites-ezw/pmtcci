<?php
class MY_Controller extends CI_Controller {

    public $data = array();
    protected $_uri = '';

    function __construct() {

        parent::__construct();

        $this->_uri = uri_string();

    }
}